def get_parts_of_string(string):
    if not isinstance(string, str):
        raise TypeError('Not a String')
    unique = ""

    for i in string:
        if i not in unique:
            unique += i


    return[unique, string[0:5], string[-5:], string[::-1] ,string[::2]]

    """
    This function takes a string as its only argument.
    It returns a list containing:

    - The string of all the unique characters
        (preserving the order is not important)
    - The first 5 characters of a string
        (If the string is shorter than 5 characters,
        return the entire string)
    - The last 5 characters of a string
        (If the string is shorter than 5 characters,
        return the entire string)
    - The entire string, backwards
    - Every other character of the string
        (The first, third, fifth, seventh etc)

    If the argument `string` is not a string, raise a TypeError
    """
