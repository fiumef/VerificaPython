def get_string_length(string):
    """
    This function takes a string as its only argument.
    It returns a number representing the number of characters in
    the string.

    If the argument `string` is not a string, raise a TypeError
    """

    if not isinstance(string, str):
        raise TypeError('Not a String')
    return len(string)
