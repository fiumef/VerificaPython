def get_string_character_num_by_type(string):
    """
    This function takes a string as its only argument.
    It returns a tuple of 3 elements:
    1. the number of alphabetic characters in the string
    2. the number of numeric characters in the string
    3. the number of characters in the string not belonging to
        the other two categories (spaces, punctuation, etc)

    (It's fine if accented letters like è end up in the last category)
    If the argument `string` is not a string, raise a TypeError
    """
    if not isinstance(string, str):
        raise TypeError('Not a String')
    alphabetic_characters = 0
    numeric_characters = 0
    other_characters= 0
    for i in string:
        if i.isnumeric():
            numeric_characters+=1
        elif i.isalpha():
            alphabetic_characters+=1
        else:
            other_characters+=1
    return (alphabetic_characters, numeric_characters, other_characters)
