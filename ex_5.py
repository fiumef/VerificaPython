def get_words_by_length(string):
    """
    This function takes a string as its only argument.
    It returns a dictionary.

    The keys of the dictionary are integers representing
    the length of words.
    The values are a list of all the words of that length.

    A word is delimited by whitespace.

    Example:

    Input string:
    "Truth does not do as much good in the world "
    "as the appearance of truth does evil."

    Expected output:
    {
        2: ["do", "as", "in", "as", "of"],
        3: ["not", "the", "the"],
        4: ["does", "much", "good", "does"],
        5: ["Truth", "world", "truth", "evil."],
        10: ["appearance"],
    }

    If the argument `string` is not a string, raise a TypeError
    """
    dictionary = {}
    if not isinstance(string, str):
        raise TypeError('Not a String')
    string_splitted = string.split()
    for i in string_splitted:
        if not len(i) in dictionary.keys():
            dictionary[len(i)] = [i]
        else:
            dictionary[len(i)].append(i)

    return dictionary


