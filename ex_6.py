def analyse_dict_values(data_dict):
    """
    This function takes a dictionary as its only argument.
    The dictionary has strings as keys, and numeric values

    It returns a dictionary containing:
    - key: "lowest" - value: the lowest value of the starting dict
    - key: "highest" - value: the highest value of the starting dict
    - key: "mean" - value: the mean of all the values in the starting dict
    - key: "total" - value: the sum of all the values in the starting dict

    Example:
    input dictionary:
    {
        "Bee": 8,
        "Puppycat": 10,
        "Finn": 5.5,
        "Jake": 9,
    }

    output dictionary:
    {
        "lowest": 5.5,
        "highest": 10,
        "mean": 8.125,
        "total": 32.5,
    }

    Raise a ValueError if the dictionary passed as input is empty
    """
    if len(data_dict)==0:
        raise ValueError("Dict is too short")
    dict_values  = data_dict.values()
    result = {}
    result["lowest"] = min(dict_values)
    result["highest"] = max(dict_values)
    result["mean"] = sum(dict_values) / len(dict_values)
    result["total"] = sum(dict_values)
    return result
