"""
Optional: Solve Advent of Code 2023, Day 1, Part 1

https://adventofcode.com/2023/day/1

    As [the Elves are] making the final adjustments, they discover that their
    calibration document (your puzzle input) has been amended by a very young Elf who
    was apparently just excited to show off her art skills. Consequently, the Elves
    are having trouble reading the values on the document.

    The newly-improved calibration document consists of lines of text; each line
    originally contained a specific calibration value that the Elves now need to
    recover. On each line, the calibration value can be found by combining the first
    digit and the last digit (in that order) to form a single two-digit number.

    For example:

        1abc2
        pqr3stu8vwx
        a1b2c3d4e5f
        treb7uchet

    In this example, the calibration values of these four lines
    are 12, 38, 15, and 77. Adding these together produces 142.

    Consider your entire calibration document.
    What is the sum of all of the calibration values?


This exercise is not covered by tests:
you're free to implement it as you like!
"""
def decoder(string):
    splitted = string.split("\n")
    decoded = ""
    total = 0;
    for i in splitted:
        for j in i:

            if j.isnumeric():
                decoded += j
                break
        for j in i[::-1]:
            if j.isnumeric():
                decoded+= j
                break
        total += int(decoded)
        decoded = ""
    return total

print(decoder("1abc2\npqr3stu8vwx\na1b2c3d4e5f\ntreb7uchet"))















