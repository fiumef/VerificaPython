import random
"""
Implement a game of guess the number.

- The user will play against the computer.
- The computer will pick a random number between 0 and 99 included
- The user needs to be able to guess.
- Tell the user if they guessed correctly, or if the number is
    higher or lower than their guess
- The user can guess 6 times. If they don't guess right,
    they lose the game.
- When the user wins or loses, show the cumulative number of wins
    and losses in that session
- The user can choose to play again, or to stop playing.

This exercise is not covered by tests:
you're free to implement it as you like!
"""
def get_number():

    while True:

        try:
            number = int(input(":"))
            if(0<=number <= 99):
                return number
            else:
                print("Invalid number, please try again.")
        except ValueError:
            print("Please try a valid number")

def check_number(number, user_number):
    if number == user_number:
        print("Congratulations!, you won")
        return "w"
    elif (number > user_number):
        print("Too Low")
        return "l"
    else:
        print("Too High")
        return "l"
def play_again():

    print("Do you want to play again?\nYes to play again, any key to quit")
    while True:
        play_again_str = input(":").casefold()
        if play_again_str == 'yes':
            return True

        else:
            return False

def play_game():
    wins = 0
    loses = 0
    lives = 6
    play = True
    games_played = 0
    number = random.randint(0, 99)
    while play:
        games_played += 1
        print("Guess a number between 0 and 99, you get 6 tries. ")
        while lives >0:

            user_number = get_number()
            result = check_number(number, user_number)
            if result == "w":
                wins += 1
                break
            else:
                lives -= 1
            if lives == 0:
                print(f"You lose!\nThe number was {number}")
                loses += 1
            print(f"{lives} tries left")
        play = play_again()
        lives = 6
    print(f"Games played: {games_played}, won {wins} times , and lost {loses} times")



play_game()