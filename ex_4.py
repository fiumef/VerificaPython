def get_list_of_initial_final_letters(string):
    """
    This function takes a string as its only argument.
    It returns a list.

    For every word in the string, the final list will contain
    a string composed of the initial and final character in
    that string.

    For this exercise, a word is any sequence of characters
    separated from other characters by whitespace.

    Example:
    Input string: "I am filled with determination!"
    Output list: ["II", "am", "fd", "wh", "d!"]

    If the argument `string` is not a string, raise a TypeError
    """
    if not isinstance(string, str):
        raise TypeError('Not a String')
    final = []
    for i in string.split():
        string_to_append = ""
        string_to_append += i[0]
        string_to_append += i[-1]
        final.append(string_to_append)
    return final
