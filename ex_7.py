import random
"""
Implement a game of rock-paper-scissors.

- The user will play against the computer.
- The user needs to be able to pick a move.
- The computer will pick a move randomly.
- Show the moves to the user
- Communicate if the game ends in victory, defeat or a draw.
- In the end, the user can choose to play again or stop.
- When the user chooses to stop,
    display the number of victories, defeats and draws over
    the entire session before terminating the program.

This exercise is not covered by tests:
you're free to implement it as you like!
"""

def pick_move(moves):
    while True:
        print("Pick a move")
        move = input(":").casefold()
        if move in moves:
            return move
        else:
            print("Invalid move")

def check_win(move, pc_move):
    if (move == 'rock' and pc_move == 'scissors') or (move == 'paper' and pc_move == 'rock') or (
        move == 'scissors' and pc_move == 'paper'):
        print(f"Great you won, you choose {move} and pc choose {pc_move}")
        return "w"
    elif(move == pc_move):
        print(f"It's a draw!! both choose {move}")
        return "d"
    else:
        print(f"Too bad you lost, you choose {move} and pc choose {pc_move}")
        return "l"

def play_again():
    print("Do you want to play again?\nYes to play again, any key to quit")
    while True:
        play_again_str = input(":").casefold()
        if play_again_str== 'yes':
            return True

        else:
            return False


def manager():
    wins =0
    looses = 0
    draws = 0
    moves = ['rock', 'paper', 'scissors']
    play = True
    while play:
        print("Rock, paper, or scissors!")

        pc_move = random.choice(moves)
        who_whon = check_win(pick_move(moves), pc_move)
        if who_whon == "w":
            wins +=1
        elif who_whon == "l":
            looses += 1
        else:
            draws += 1
        play = play_again()
    print(f"Thanks for playing!, you win {wins} times, lost {looses} times, draw {draws} times")


manager()

